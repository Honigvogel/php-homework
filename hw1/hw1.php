<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="main.css">
    <title>Hello world Arzamasova 211-329</title>
</head>
<body>
    <header class="header">
        <img class="logo" src="img/polytech_logo.png" alt="Logotype of Moscow Polytech">
        <p class="title">Hello, World!</p>
    </header>
    <main>
        <?php echo 'Привет мир!'; ?>
    </main>
    <footer>
        <p>Создание веб-страницы с динамическим контентом</p>
    </footer>
</body>
</html>