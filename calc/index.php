<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculator</title>
</head>
<body>    

<?php

require 'f.php';

$f_add = function ($a, $b){
    if (is_null($a) || is_null($b))	return NULL;	
    return (float)($a + $b);
};
$f_dec = function ($a, $b){
    if (is_null($a) || is_null($b))	return NULL;	
    return (float)($a - $b);
};
$f_mul = function ($a, $b){
    if (is_null($a) || is_null($b))	return NULL;	
    return (float)($a*$b);
};
$f_div = function ($a, $b){
    if (is_null($a) || is_null($b) || $b == 0)	return NULL;	
    return (float)($a/$b);
};

$arr_func = array($f_add,$f_dec,$f_mul,$f_div); // массив арифметических функций 
$arr_op   = array('+','-', '*', '/'); //массив знаков арифметических операций

function op_strrpos($s, $c){
    $k = strlen($s) - 1;
    while($k>0) { // самый первый символ исключается из поиска	
        if ($s[$k] == $c) { 
            if (!($s[$k-1] == '+' || $s[$k-1] == '-' || $s[$k-1] == '*' || $s[$k-1] == '/')) return $k; //если это не унарные знаки + и -, то вернуть найденную позицию
        }		
        $k--;
    }
    return false;
}

function calc($s){
    global $arr_func, $arr_op;
    $s = trim($s);	
    if (is_numeric($s))	return (float)$s;
    
    if ( strpos($s,'(') !== false ) {// обработка скобок
        $k1 = strrpos($s,'('); // поиск последней открывающейся скобки
        $a = substr($s,$k1+1);
        if ( strpos($a,')') === false ) return NULL; // если нет закрывающей скобки, то ошибка
        $k2 = strpos($a,')'); // поиск закрывающей скобки
        $a = substr($a,0,$k2); // получить выражение в скобках
        $res = calc($a);
        if ($res === NULL) return NULL;		
        return calc( substr($s,0,$k1) . strval($res) . substr($s,$k1+$k2+2) ); // замена выражение в скобках на результат и снова вычислить все выражение
    }
        
    for ($i = 0; $i < 4; $i++) { // обработка операций +,-,*,/
        if ( op_strrpos($s,$arr_op[$i]) !== false ) {
            $k = op_strrpos($s,$arr_op[$i]); // поиск арифметических операций в заданной строке
            $a = substr($s,0,$k); // первый операнд бинарной операции
            $b = substr($s,$k+1); // второй операнд бинарной операции
            $res = $arr_func[$i](calc($a),calc($b)); //  вычислить результат с помощью пользовательской функции
            return $res;
        }		
    }	
    return NULL;
}


$result_val = '';
if (!empty($_POST["calc"])) 
{       
    file_put_contents('expression.txt', $_POST["calc"]);

    if (!empty($_POST["f"])){
        $func = $_POST["f"];
        $exp = file_get_contents('expression.txt');
        $res = $func( calc($exp) );
        $response = ($res === NULL) ? "Error!" : strval($res);
    }
    else{
        $res = calc($_POST["calc"]);
        $response = ($res === NULL) ? "Error!" : strval($res);
    }

    $result_val = $response;
}
?>

    <form action="index.php" method="POST">
        <input name="calc" id="calc" type="text" value="<?php echo $result_val?>" readOnly><br>
        <button onclick="tr('f_sin')">sin</button>
        <button onclick="tr('f_cos')">cos</button>
        <button onclick="tr('f_tan')">tg</button><br>
        <button type="button" onclick="wr('(')">(</button>
        <button type="button" onclick="wr(')')">)</button>
        <button type="button" onclick="clr()">C</button><br>
        <button type="button" onclick="wr('7')">7</button>
        <button type="button" onclick="wr('8')">8</button>
        <button type="button" onclick="wr('9')">9</button><br>
        <button type="button" onclick="wr('4')">4</button>
        <button type="button" onclick="wr('5')">5</button>
        <button type="button" onclick="wr('6')">6</button><br>
        <button type="button" onclick="wr('1')">1</button>
        <button type="button" onclick="wr('2')">2</button>
        <button type="button" onclick="wr('3')">3</button><br>
        <button type="button" onclick="wr('0')">0</button>
        <button type="button" onclick="wr('+')">+</button>
        <button type="button" onclick="wr('-')">-</button><br>
        <button type="button" onclick="wr('/')">/</button>
        <button type="button" onclick="wr('*')">*</button>
        <input type="hidden" id='f' name='f' value=''>
        <button type="submit">=</button>
        <br>
        
    

        
        
    </form>
    <script src="main.js"></script>
</body>
</html>