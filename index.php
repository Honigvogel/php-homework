<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $a = 2; 
        $b = 2.0;
        $c = '2';
        $d = 'two';
        $g = true;
        $f = false;
        
        echo(gettype($a), (string)$a);
        echo(gettype($b), (string)$b);
        echo(gettype($c), (string)$c);
        echo(gettype($d), (string)$d);
        echo(gettype($g), (string)$g);
        echo(gettype($f), (string)$f);

    ?>
</body>
</html>